import React, { useEffect, useState } from 'react';


function HatForm () {
    const [styleName, setStyle] = useState('');
    const [fabric, setFabric] = useState('');
    const [color, setColor] = useState('');
    const [url, setPicture] = useState('');
    const [location, setLocation] = useState(false);
    const [locations, setLocations] = useState([]);

    const styleChange = (event) => {
        const valueStyle = event.target.value;
        setStyle(valueStyle);
    }

    const fabricChange = (event) => {
        const valueFabric = event.target.value;
        setFabric(valueFabric);
    }

    const colorChange = (event) => {
        const valueColor = event.target.value;
        setColor(valueColor);
    }

    const pictureChange = (event) => {
        const valuePicture = event.target.value;
        setPicture(valuePicture);
    }

    const locationChange = (event) => {
        const valueLocation = event.target.value;
        setLocation(valueLocation);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data ={};
        data.style_name = styleName;
        data.fabric = fabric;
        data.color = color;
        data.url = url
        data.location = location;
        console.log(data)

        const hatUrl = `http://localhost:8090/api/locations/${location}/hats/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const hatResponse = await fetch(hatUrl, fetchConfig)
        if (hatResponse.ok) {
            const newHats = await hatResponse.json();
            console.log(newHats)

            setStyle('');
            setFabric('');
            setColor('');
            setLocation([]);

            window.location.replace("/hats");
        }
    }
    const fetchData = async () => {
        const locationUrl = 'http://localhost:8100/api/locations/';
        const response = await fetch(locationUrl);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }


useEffect (() => {
    fetchData();
}, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Hat</h1>
                <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    <input onChange={styleChange} placeholder="Style name" required type="text" name="style_name" id="style_name" className="form-control" value={styleName}/>
                    <label htmlFor="name">Style</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={fabricChange} placeholder="Fabric type" required type="text" name="fabric" id="fabric" className="form-control" value={fabric}/>
                    <label htmlFor="name">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={colorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color}/>
                    <label htmlFor="name">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={pictureChange} placeholder="Picture url" type="text" name="url" id="url" className="form-control" value={url}/>
                    <label htmlFor="name">Picture url</label>
                </div>
                <div className="mb-3">
                    <select required onChange={locationChange} id="location" name="location" className="form-select" value={location}>
                        <option value="">Choose a location</option>
                        {locations.map(location => {
                            return (
                                <option key={location.href} value={location.id}>
                                    {location.closet_name}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    );
}
export default HatForm;
