import './index.css';
import { useState } from 'react';

export default function ShoeList(props) {

    const [shoes, setShoes] = useState(props.shoes);

    async function handleDeleteShoe(shoe) {

        const url = `http://localhost:8080/api/shoes/${shoe}/`
        const response = await fetch(url, { method: 'DELETE' })
        if (response.ok) {
            const data = await response.json();
            await updateList();
        }
        window.location.reload();
    }

    async function updateList() {

        const url = 'http://localhost:8080/api/shoes/'
        const response = await fetch(url, { method: 'GET' })
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        }
    }


    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Bin</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe => {
                        return (
                            <tr key={shoe.shoe_bin + shoe.id}>
                                <td className='ahh'>{shoe.manufacturer}</td>
                                <td className='ahh'>{shoe.model}</td>
                                <td className='ahh'>{shoe.color}</td>
                                <td className='ahh'><img src={shoe.picture} className="image" alt="shoe" /></td>
                                <td className='ahh'>{shoe.shoe_bin}</td>
                                <td className='ahh'><button className="button" onClick={() => handleDeleteShoe(shoe.id)}>X</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}
