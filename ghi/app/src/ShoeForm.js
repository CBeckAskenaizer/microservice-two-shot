import { useEffect, useState } from "react";

//Form to CREATE a shoe!! POST requst to http://localhost:8080/api/shoes/

export default function ShoeForm() {
    const [bins, setBins] = useState([]);

    const [manufacturer, setManufacturer] = useState('');
    const [model, setModel] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [shoeBin, setShoeBin] = useState('');


    function handleManufacturerChange(event) {
        const value = event.target.value
        setManufacturer(value);
    }
    function handleModelChange(event) {
        const value = event.target.value
        setModel(value);
    }
    function handleColorChange(event) {
        const value = event.target.value
        setColor(value);
    }
    function handlePictureChange(event) {
        const value = event.target.value
        setPicture(value);
    }
    function handleBinChange(event) {
        const value = event.target.value
        setShoeBin(value);
    }

    const handleSubmit = async (event) => {
        // async request which may result error
        event.preventDefault();
        const data = {};

        data.manufacturer = manufacturer;
        data.model = model;
        data.color = color;
        data.picture = picture;
        data.shoe_bin = shoeBin;

        console.log(data);

        const shoeUrl = `http://localhost:8080${shoeBin}shoes/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json',
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);
            setManufacturer('');
            setModel('');
            setColor('');
            setPicture('');
            setPicture('');

            window.location.replace("/shoes");
        }
    }

    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/"

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-attendee-form">
                                <h1 className="card-title">Add a new shoe!</h1>
                                <p className="mb-3">
                                    Add a shoe to an existing bin
                                </p>
                                {/* <div className="row"> */}
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input value={manufacturer} onChange={handleManufacturerChange} required placeholder="Manufacturer name" type="text" id="manufacturer"
                                            name="manufacturer" className="form-control" />
                                        <label htmlFor="manufacturer">Manufacturer</label>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input value={model} onChange={handleModelChange} required placeholder="Model name" type="model" id="model"
                                            name="model" className="form-control" />
                                        <label htmlFor="model">Model</label>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input value={color} onChange={handleColorChange} required placeholder="Shoe color" type="text" id="color"
                                                name="color" className="form-control" />
                                            <label htmlFor="color">Shoe color</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input value={picture} onChange={handlePictureChange} required placeholder="Picture" type="picture" id="picture"
                                                name="picture" className="form-control" />
                                            <label htmlFor="picture">Picture</label>
                                        </div>
                                    </div>
                                </div>
                                {/* </div> */}
                                <p className="mb-3">
                                    Place shoes in which bin?
                                </p>
                                {/* <div className={spinnerClasses} id="loading-conference-spinner">
                                    <div className="spinner-grow text-secondary" role="status">
                                        <span className="visually-hidden">Loading...</span>
                                    </div>
                                </div> */}
                                <div className="mb-3">
                                    <select input={shoeBin} onChange={handleBinChange} name="shoe_bin" id="shoe_bin" required>
                                        <option value="">Choose a Bin</option>
                                        {bins.map(bin => {
                                            return (
                                                <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                                            );
                                        })}
                                    </select>
                                </div>
                                <button className="btn btn-lg btn-primary">Create shoe</button>
                            </form>
                            <div className="alert alert-success d-none mb-0" id="success-message">
                                Congratulations! You're all signed up!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )


};
