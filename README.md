# Wardrobify

Team:

- Person 1 - Stephen Zhu - Hats
- Person 2 - Christina Beck-Askenaizer - Shoes

## Design

## Shoes microservice

I have my shoe model which consists of the shoe's details. Inside that model, I have a foreign key called "shoe_bin" that connects to my BinVO model. The BinVO model is my "value object". Each shoe in my database is linked to a bin object. Bins can have multiple shoes, shoes can only be in one bin.

In order to retrieve data into my shoes microservce, I used polling to periodically pull from the wardrobe microservice.

## Hats microservice

The Hats model sets the attributes for defining the hat when creating the object.  Being in a microservice the LocationVO is used and called when creating a hat because it takes the information provided and used to create an API in which polling can be used when communicating with the wardrobe microservice for the correct data pull.
