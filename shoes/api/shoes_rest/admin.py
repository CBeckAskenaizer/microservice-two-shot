from django.contrib import admin
from shoes_rest.models import Shoe, BinVO

class ShoeAdmin(admin.ModelAdmin):
    list_display = (
        "manufacturer",
        "model",
        "color",
        "picture",
        "shoe_bin",
    )

class BinVOAdmin(admin.ModelAdmin):
    list_display = (
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",
    )

admin.site.register(Shoe, ShoeAdmin)
admin.site.register(BinVO, BinVOAdmin)
