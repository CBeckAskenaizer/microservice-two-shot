from django.db import models
from django.urls import reverse

# Create your models here.
class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    bin_number = models.PositiveSmallIntegerField(null = True)
    bin_size = models.PositiveSmallIntegerField(null = True)

    def __str__(self):
        return self.closet_name

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200, null=True, blank=True)
    model = models.CharField(max_length=200, null=True, blank=True)
    color = models.CharField(max_length=200, null=True, blank=True)
    picture = models.CharField(max_length=200, null=True,blank=True)

    shoe_bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.manufacturer


    def get_api_url(self):
        return reverse("api_show_attendee", kwargs={"pk": self.pk})

    class Meta:
        ordering = ("manufacturer", "model", "color", "picture", "shoe_bin")
